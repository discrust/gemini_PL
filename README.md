# gemini_PL.gmi 

Polska część Geminispace.

## Co to za lista?

Lista *gemini_pl.gmi* to inicjatywa mająca na celu ułatwienie dotarcia do polskich autorów w przestrzeni Gemini (ang. Geminispace). Lista jest utrzymywana przez społeczność i dostępna dla każdego, który może ją publikować w ramach swojej kapsuły Gemini. 

## Co to jest Gemini i Geminispace?

Odpowiedzi dostępne w [Project Gemini FAQ](https://gemini.circumlunar.space/docs/faq.gmi)

## Kto zarządza listą?

Każdy ma prawo do modyfikacji listy *gemini_pl.gmi* poprzez zgłoszenie tzw. Pull Request do repozytorium

Instrukcja wykonania powyższego znajduje się w serwisie, gdzie zlokalizowane jest repozytorium https://docs.codeberg.org/collaborating/pull-requests-and-git-flow/ (należy wykonać polecenie Fork, wprowadzić zmiany na swojej gałęzi (tzw. branch) i wykonać polecenie New Pull Request.

W przypadku braku możliwości wykonania powyższych kroków technicznych, możliwe jest utworzenie zgłoszenia wchodząc na adres https://codeberg.org/szczezuja/gemini_PL/issues Ręczne zgłoszenia zmian będą nanoszone w miarę dostępności. 

## Kto i jak może wykorzystać listę?

Lista *gemini_pl.gmi* dostępna jest na licencji opisanej w pliku LICENSE oraz dostępnej w pod adresem https://creativecommons.org/licenses/by-nc/4.0/deed.pl